from .logger import LOGGER
from .storage import NonVolatileQueue
from .task_providers.secondary import SecondaryTaskProvider
from .search_results_sorter import SearchResultsSorter

import Pyro4


@Pyro4.expose
class MasterService:
    def __init__(self):
        self._nodes = set()

        # FIXME: paths in config
        self._search_results = NonVolatileQueue("search_results")
        self._clones = NonVolatileQueue("clones")

    def connect(self, uri):
        LOGGER.info("New node connected! Node URI = {}".format(uri))
        self._nodes.add(uri)

    # TODO: Make search task getting
    def get_task_search(self):
        '''
        get_task_search -> Tuple(query,page)
        '''
        pass

    def get_task_clone(self):
        name, item = self._search_results.take()
        if name is None:
            return
        if item is None:
            return self._search_results.dispose(name)
        size, url, name = item
        return name, url

    # recieves dict of 〈repo name〉 keys and 〈clone url, size, stars〉 values
    def finish_search(self, results):
        LOGGER.info("Stats about repros stored in: %s" % search_results)
        for name in results:
            if self._clones.contains(name):
                continue
            url, size, stars = results[name]
            weight = size / stars if stars else size * size
            self._search_results.put(name, weight, (size, url, name))

    # receives dict of 〈repo name〉 keys and 〈python file names set〉 values
    def finish_clone(self, results):
        LOGGER.info("Information about cloning in: %s" % cloned_rep)
        for name, files in results.items():
            self._search_results.dispose(name)
            self._clones.put(name, files)

    # TODO: made finish event
    def finish_build_index(self):
        '''
        Event runned when build finishes
        :return: None
        '''
        pass

@Pyro4.expose
class ProxyServer:
    def __init__(self, config):
        self._config = config

    def search(self, query):
        tasks = []
        results = set()
        for node in self._nodes:
            master_client = Pyro4.Proxy(node)
            master_client._PyroAsync()
            tasks.append(master_client.search(query))
        for task in tasks:
            task.wait()
            results.add(task.result)

        response = SearchResultsSorter(list(results), self._config)
        return response.sort_by_relevance(query)
